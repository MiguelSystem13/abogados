# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

class Email(models.Model):
      Nombre = models.CharField(max_length=150)
      Telefono = models.CharField(max_length=150)
      email = models.EmailField()
      direccion = models.CharField(max_length=250)
      mensaje = models.CharField(max_length=250)


class Abogado(models.Model):
    id_abogado = models.AutoField(primary_key=True)
    estado = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'abogado'


class About(models.Model):
    id_about = models.AutoField(primary_key=True)
    imagen_uno=models.ImageField(max_length=150, upload_to="about")
    imagen_dos = models.ImageField(max_length=150, upload_to="about")
    imagen_tres = models.ImageField(max_length=150, upload_to="about")
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')
    titulo_uno = models.CharField(max_length=150)
    titulo_dos = models.CharField(max_length=150)
    titulo_tres = models.CharField(max_length=150)
    desc_uno = models.CharField(max_length=160)
    desc_dos = models.CharField(max_length=160)
    desc_tres = models.CharField(max_length=160)
    titulo_izq = models.CharField(max_length=150)
    desc_izq = models.CharField(max_length=600)
    imagen_historia = models.ImageField(max_length=150, upload_to="about")
    imagen_encabezado_historia= models.ImageField(max_length=150, upload_to="about")
    descripcion_historia = models.CharField(max_length=1500)
    imagen_firma = models.ImageField(max_length=150, upload_to="about")

    class Meta:
        managed = False
        db_table = 'about'


class Area(models.Model):
    id_area = models.AutoField(primary_key=True)
    nombre_area = models.CharField(max_length=100)
    imagen_uno = models.ImageField(max_length=150, upload_to="area")
    imagen_dos = models.ImageField(max_length=150, upload_to="area")
    imagen_tres = models.ImageField(max_length=150, upload_to="area")
    descripcion = models.CharField(max_length=600)
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')

    class Meta:
        managed = False
        db_table = 'area'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Capacidad(models.Model):
    id_capacidad = models.AutoField(primary_key=True)
    imagen = models.ImageField(max_length=150, upload_to="capacidades")
    titulo = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=152)
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')

    class Meta:
        managed = False
        db_table = 'capacidad'


class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')
    imagen = models.ImageField(max_length=150, upload_to="cliente")
    enlace = models.CharField(max_length=300)

    class Meta:
        managed = False
        db_table = 'cliente'

class Cita(models.Model):
    id_cita = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=150)
    nombre_autor = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=200)
    imagen_perfil = models.ImageField(max_length=150, upload_to="cita")
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')

    class Meta:
        managed = False
        db_table = 'cita'

class Contactenos(models.Model):
    id_contactenos = models.AutoField(primary_key=True)
    telefono_uno = models.CharField(max_length=50)
    telefono_dos = models.CharField(max_length=50, blank=True, null=True)
    telefono_tres = models.CharField(max_length=50, blank=True, null=True)
    latitud = models.CharField(max_length=300)
    longitud = models.CharField(max_length=300)
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')

    class Meta:
        managed = False
        db_table = 'contactenos'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class EquipoMiembro(models.Model):
    id_equipo = models.AutoField(primary_key=True)
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')
    imagen = models.ImageField(max_length=150, upload_to="equipo")
    nombre = models.CharField(max_length=20)
    facebok = models.CharField(max_length=350)
    twitter = models.CharField(max_length=350)
    especialidad = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=2000)
    titulo_descripcion = models.CharField(max_length=150)

    class Meta:
        managed = False
        db_table = 'equipo_miembro'

class Encabezado(models.Model):
    id_encabezado = models.AutoField(primary_key=True)
    imagen_encabezado_noticia = models.ImageField(max_length=100, upload_to="encabezado")
    imagen_encabezado_acerca = models.ImageField(max_length=100, upload_to="encabezado")
    imagen_encabezado_area = models.ImageField(max_length=100, upload_to="encabezado")
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')

    class Meta:
        managed = False
        db_table = 'encabezado'



class HeaderImagen(models.Model):
    id_headerimagen = models.AutoField(primary_key=True)
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')
    imagen = models.ImageField(max_length=150, upload_to="header")
    titulo = models.CharField(max_length=300)
    descripcion = models.CharField(max_length=500)
    nombre_enlace = models.CharField(max_length=50, blank=True, null=True)
    direccion_enlace = models.CharField(max_length=300, blank=True, null=True)
    # imagen_encabezado_noticia = models.ImageField(max_length=150, upload_to="static/imagenes_noticias")
    # imagen_encabezado_area = models.ImageField(max_length=150, upload_to="static/img_area")
    # imagen_encabezado_acerca = models.ImageField(max_length=150, upload_to="static/img_about")

    class Meta:
        managed = False
        db_table = 'header_imagen'


class Item(models.Model):
    id_item = models.AutoField(primary_key=True)
    nombre_item = models.CharField(max_length=50)
    id_area = models.ForeignKey(Area, models.DO_NOTHING, db_column='id_area')

    class Meta:
        managed = False
        db_table = 'item'


class Noticia(models.Model):
    id_noticia = models.AutoField(primary_key=True)
    imagen = models.ImageField(max_length=300, upload_to="noticias")
    titulo = models.CharField(max_length=300)
    # Subir imagenes al servidor ckeditor
    contenido = RichTextUploadingField()
    fecha = models.DateField()
    descripcion = RichTextField()
    palabras_clave = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'noticia'


class Parallax(models.Model):
    id_parallax = models.AutoField(primary_key=True)
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')
    titulo = models.CharField(max_length=150)
    descripcion = models.CharField(max_length=300)
    imagen = models.ImageField(max_length=150, upload_to="parallax")

    class Meta:
        managed = False
        db_table = 'parallax'


class ParallaxDos(models.Model):
    id_parallaxdos = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=200)
    imagen = models.ImageField(max_length=150, upload_to="parallax_dos")
    imagen_parallax_tres = models.ImageField(max_length=150, upload_to="parallax_dos")
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')

    class Meta:
        managed = False
        db_table = 'parallax_dos'

# class ParallaxTres(models.Model):
#     id_parallaxtres = models.AutoField(primary_key=True)
#     titulo = models.CharField(max_length=200)
#     imagen = models.ImageField(max_length=150, upload_to="static/img_parallax_tres")
#     id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')

#     class Meta:
#         managed = False
#         db_table = 'parallax_tres'


class SeccionUno(models.Model):
    id_seccionuno = models.AutoField(primary_key=True)
    titulo_izq = models.CharField(max_length=200)
    titulo_der = models.CharField(max_length=50)
    descripcion_izq = models.CharField(max_length=200)
    imagen_izq = models.ImageField(max_length=150, upload_to="seccion_uno")
    imagen_der = models.ImageField(max_length=150, upload_to="seccion_uno")
    descripcion_der = models.CharField(max_length=350)
    descripcion_der_sub = models.CharField(max_length=300)
    titulo_der_sub = models.CharField(max_length=50)
    id_abogado = models.ForeignKey(Abogado, models.DO_NOTHING, db_column='id_abogado')

    class Meta:
        managed = False
        db_table = 'seccion_uno'
