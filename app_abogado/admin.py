# vim: set fileencoding=utf-8 :
from django.contrib import admin

from . import models


class AbogadoAdmin(admin.ModelAdmin):

    list_display = ('id_abogado', 'estado')
    list_filter = ('estado',)


class AboutAdmin(admin.ModelAdmin):

    list_display = (
        'id_about',
        'imagen_uno',
        'imagen_dos',
        'imagen_tres',
        'id_abogado',
        'titulo_uno',
        'titulo_dos',
        'titulo_tres',
        'desc_uno',
        'desc_dos',
        'desc_tres',
        'titulo_izq',
        'desc_izq',
        'imagen_historia',
        'imagen_encabezado_historia',
        'descripcion_historia',
        'imagen_firma',
    )
    list_filter = ('id_abogado',)


class AreaAdmin(admin.ModelAdmin):

    list_display = (
        'id_area',
        'nombre_area',
        'imagen_uno',
        'imagen_dos',
        'imagen_tres',
        'descripcion',
        'id_abogado',
    )
    list_filter = ('id_abogado',)


class AuthGroupAdmin(admin.ModelAdmin):

    list_display = (u'id', 'name')
    search_fields = ('name',)


class AuthGroupPermissionsAdmin(admin.ModelAdmin):

    list_display = (u'id', 'group', 'permission')
    list_filter = ('group', 'permission')


class AuthPermissionAdmin(admin.ModelAdmin):

    list_display = (u'id', 'name', 'content_type', 'codename')
    list_filter = ('content_type',)
    search_fields = ('name',)


class AuthUserAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'password',
        'last_login',
        'is_superuser',
        'username',
        'first_name',
        'last_name',
        'email',
        'is_staff',
        'is_active',
        'date_joined',
    )
    list_filter = (
        'last_login',
        'is_superuser',
        'is_staff',
        'is_active',
        'date_joined',
    )


class AuthUserGroupsAdmin(admin.ModelAdmin):

    list_display = (u'id', 'user', 'group')
    list_filter = ('user', 'group')


class AuthUserUserPermissionsAdmin(admin.ModelAdmin):

    list_display = (u'id', 'user', 'permission')
    list_filter = ('user', 'permission')


class CapacidadAdmin(admin.ModelAdmin):

    list_display = (
        'id_capacidad',
        'imagen',
        'titulo',
        'descripcion',
        'id_abogado',
    )
    list_filter = ('id_abogado',)


class ClienteAdmin(admin.ModelAdmin):

    list_display = ('id_cliente', 'id_abogado', 'imagen', 'enlace')
    list_filter = ('id_abogado',)

class CitaAdmin(admin.ModelAdmin):

    list_display = ('id_cita',
        'titulo',
        'id_abogado',
        'nombre_autor',
        'ciudad',
        'imagen_perfil',
    )
    list_filter = ('id_abogado',)


class ContactenosAdmin(admin.ModelAdmin):

    list_display = (
        'id_contactenos',
        'telefono_uno',
        'telefono_dos',
        'telefono_tres',
        'latitud',
        'longitud',
        'id_abogado',
    )
    list_filter = ('id_abogado',)


class DjangoAdminLogAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'action_time',
        'object_id',
        'object_repr',
        'action_flag',
        'change_message',
        'content_type',
        'user',
    )
    list_filter = ('action_time', 'content_type', 'user')


class DjangoContentTypeAdmin(admin.ModelAdmin):

    list_display = (u'id', 'app_label', 'model')


class DjangoMigrationsAdmin(admin.ModelAdmin):

    list_display = (u'id', 'app', 'name', 'applied')
    list_filter = ('applied',)
    search_fields = ('name',)


class DjangoSessionAdmin(admin.ModelAdmin):

    list_display = ('session_key', 'session_data', 'expire_date')
    list_filter = ('expire_date',)


class EquipoMiembroAdmin(admin.ModelAdmin):

    list_display = (
        'id_equipo',
        'id_abogado',
        'imagen',
        'nombre',
        'facebok',
        'twitter',
        'especialidad',
        'descripcion',
        'titulo_descripcion',
    )
    list_filter = ('id_abogado',)

class EncabezadoAdmin(admin.ModelAdmin):

    list_display = (
        'id_encabezado',
        'id_abogado',
        'imagen_encabezado_noticia',
        'imagen_encabezado_acerca',
        'imagen_encabezado_area',

    )
    list_filter = ('id_abogado',)

class HeaderImagenAdmin(admin.ModelAdmin):

    list_display = (
        'id_headerimagen',
        'id_abogado',
        'imagen',
        'titulo',
        'descripcion',
        'nombre_enlace',
        'direccion_enlace',
    )
    list_filter = ('id_abogado',)


class ItemAdmin(admin.ModelAdmin):

    list_display = ('id_item', 'nombre_item', 'id_area')
    list_filter = ('id_area',)


class NoticiaAdmin(admin.ModelAdmin):

    list_display = (
        'id_noticia',
        'imagen',
        'titulo',
        'fecha',
        'palabras_clave',
    )
    list_filter = ('fecha',)


class ParallaxAdmin(admin.ModelAdmin):

    list_display = (
        'id_parallax',
        'id_abogado',
        'titulo',
        'descripcion',
        'imagen',
    )
    list_filter = ('id_abogado',)


class ParallaxDosAdmin(admin.ModelAdmin):

    list_display = (
        'id_parallaxdos',
        'titulo',
        'imagen',
        'imagen_parallax_tres',
        'id_abogado',
    )
    list_filter = ('id_abogado',)


class SeccionUnoAdmin(admin.ModelAdmin):

    list_display = (
        'id_seccionuno',
        'titulo_izq',
        'titulo_der',
        'descripcion_izq',
        'imagen_izq',
        'imagen_der',
        'descripcion_der',
        'descripcion_der_sub',
        'titulo_der_sub',
        'id_abogado',
    )
    list_filter = ('id_abogado',)


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.Abogado, AbogadoAdmin)
_register(models.About, AboutAdmin)
_register(models.Area, AreaAdmin)
_register(models.AuthGroup, AuthGroupAdmin)
_register(models.AuthGroupPermissions, AuthGroupPermissionsAdmin)
_register(models.AuthPermission, AuthPermissionAdmin)
_register(models.AuthUser, AuthUserAdmin)
_register(models.AuthUserGroups, AuthUserGroupsAdmin)
_register(models.AuthUserUserPermissions, AuthUserUserPermissionsAdmin)
_register(models.Capacidad, CapacidadAdmin)
_register(models.Cliente, ClienteAdmin)
_register(models.Cita, CitaAdmin)
_register(models.Contactenos, ContactenosAdmin)
_register(models.DjangoAdminLog, DjangoAdminLogAdmin)
_register(models.DjangoContentType, DjangoContentTypeAdmin)
_register(models.DjangoMigrations, DjangoMigrationsAdmin)
_register(models.DjangoSession, DjangoSessionAdmin)
_register(models.EquipoMiembro, EquipoMiembroAdmin)
_register(models.Encabezado, EncabezadoAdmin)
_register(models.HeaderImagen, HeaderImagenAdmin)
_register(models.Item, ItemAdmin)
_register(models.Noticia, NoticiaAdmin)
_register(models.Parallax, ParallaxAdmin)
_register(models.ParallaxDos, ParallaxDosAdmin)
_register(models.SeccionUno, SeccionUnoAdmin)
