from django.conf.urls import *
from . import views
from django.conf.urls.static import static
from django.conf import settings

# from django.conf.urls.static import static
# from django.conf import settings

urlpatterns = [
	url(r'^$', views.index, name='index' ),
	#url(r'^areas$', views.areas, name='areas'),
	url(r'^about/$', views.about, name='acerca' ),
	url(r'^contact/$', views.contact, name='contactos' ),
	url(r'^history/$', views.history, name='historia' ),
	url(r'^news/$', views.news, name='noticias' ),
	# url(r'^single_news/$', views.single_news, name='single_news' ),
    url(r'^single_news/(?P<pk>[0-9]+)/$', views.single_news, name='single_news'),
    url(r'^listeam/(?P<id_equipo>[0-9]+)/$', views.list_team, name='listeam'),
	url(r'^team/$', views.team, name='equipo' ),
	url(r'^staff_single/$', views.staff_single, name='perfil'),
	# url(r'^staff_single/$', views.staf_single, name='staff_single' ),
	url(r'^services/$', views.services, name='area' ),
	url(r'^json/(?P<inf>[0-9]+)/(?P<sup>[0-9]+)/$', views.json_noticias, name='json'),
	url(r'^email/$', views.sendEmail, name='email'),
	url(r'^grappelli/', include('grappelli.urls')),
	url(r'^emaila/$', views.sendEmailA, name='emaila'),


]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urlpatterns += staticfiles_urlpatterns()
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)