# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
import json
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.shortcuts import render
from django.core.mail import EmailMultiAlternatives

from django.conf import settings

from app_abogado.models import *

# Create your views here.
def index(request):
	header = HeaderImagen.objects.all()#llama a todas la clase
	lista=[]
	for a in header:
		titulo = a.titulo
		imagen = str(a.imagen)
		nombre_enlace = a.nombre_enlace
		direccion = a.direccion_enlace
		print imagen
		lista.append({'titulo':titulo,'imagen':imagen,'enlace':nombre_enlace,'dir':direccion})

	seccionuno = SeccionUno.objects.all()
	listaSecUno =[]
 	for a in seccionuno:
		imagen_izq = str(a.imagen_izq)
		imagen_der = str(a.imagen_der)
		titulo_der = a.titulo_der
		titulo_izq = a.titulo_izq
		descripcion_der = a.descripcion_der
		descripcion_izq = a.descripcion_izq
		titulo_der_sub = a.titulo_der_sub
		descripcion_der_sub = a.descripcion_der_sub

	listaSecUno.append({'imagen_der':imagen_der, 'imagen_izq':imagen_izq, 'titulo_der':titulo_der, 'titulo_izq':titulo_izq, 'descripcion_izq':descripcion_izq, 'titulo_der_sub':titulo_der_sub, 'descripcion_der':descripcion_der, 'descripcion_der_sub':descripcion_der_sub})

	capacidad = Capacidad.objects.all()
	lista2=[]
 	for c in capacidad:
		titulo = c.titulo
		descripcion=c.descripcion
		imagen = str(c.imagen)

		lista2.append({'titulo':titulo,'descripcion':descripcion,'imagen':imagen})

	parallax = Parallax.objects.all()
	listaPar =[]
 	for p in parallax:
		imagen = str(p.imagen)
		titulo = p.titulo
		descripcion = p.descripcion
		listaPar.append({'imagen':imagen, 'titulo':titulo, 'descripcion':descripcion})

	parallaxdos = ParallaxDos.objects.all()
	listaParDos=[]
 	for a in parallaxdos:
		imagen = str(a.imagen)
		imagen_parallax_tres = str(a.imagen_parallax_tres)
		titulo = a.titulo
	listaParDos.append({'imagen':imagen, 'titulo':titulo, 'imagen_parallax_tres':imagen_parallax_tres})

	equipo = EquipoMiembro.objects.all()
	listaEqui=[]
 	for a in equipo:
		imagen = str(a.imagen)
		nombre = a.nombre
		facebok = a.facebok
		twitter = a.twitter
		especialidad = a.especialidad
		descripcion = a.descripcion
		listaEqui.append({'nombre':nombre, 'imagen':imagen, 'facebok':facebok, 'twitter':twitter, 'especialidad':especialidad, 'descripcion':descripcion})

	# 'nombre':nombre, 'facebook':facebook, 'twitter':twitter, 'especialidad':especialidad, 'descripcion':descripcion

	# 'imagen':imagen,
	cliente = Cliente.objects.all()
	listaCli =[]
 	for a in cliente:
		imagen = str(a.imagen)
		enlace = a.enlace
		listaCli.append({'imagen':imagen, 'enlace':enlace})


	cliente = Cliente.objects.all()
	listaCli =[]
 	for a in cliente:
		imagen = str(a.imagen)
		enlace = a.enlace
		listaCli.append({'imagen':imagen, 'enlace':enlace})

	area = Area.objects.all()
	listaAre =[]
	for a in area:
 		area_id= a.pk
		nombre_area = a.nombre_area
		descripcion = a.descripcion
		print(area_id)
		listaAre.append({'area_id':area_id, 'nombre_area':nombre_area, 'descripcion':descripcion})

	cita = Cita.objects.all()
	listaCita = []
	for c in cita:
		titulo = c.titulo
		nombre_autor = c.nombre_autor
		ciudad = c.ciudad
		imagen_perfil = str(c.imagen_perfil)
		listaCita.append({'titulo':titulo, 'nombre_autor':nombre_autor, 'ciudad':ciudad, 'imagen_perfil':imagen_perfil})


	ctx = {'header':lista,'seccionuno':seccionuno, 'capacidad':lista2, 'parallax':parallax, 'parallaxdos':parallaxdos, 'cliente':listaCli, 'parallax':listaPar, 'parallaxdos':listaParDos, 'seccionuno':listaSecUno, 'equipo':listaEqui,'area':listaAre,'cita':listaCita}
	return render(request, 'index.html', ctx)

def about(request):
	about = About.objects.all()
	listaAb =[]
 	for a in about:
		imagen_uno = str(a.imagen_uno)
		imagen_dos = str(a.imagen_dos)
		imagen_tres = str(a.imagen_tres)
		# imagen_about = str(a.imagen_about)[7:]
		titulo_uno = a.titulo_uno
		titulo_dos = a.titulo_dos
		titulo_tres = a.titulo_tres
		titulo_izq = a.titulo_izq
		desc_uno = a.desc_uno
		desc_dos = a.desc_dos
		desc_tres = a.desc_tres
		desc_izq =  a.desc_izq
		# enlace = a.enlace
		listaAb.append({'imagen_uno':imagen_uno, 'titulo_uno':titulo_uno, 'titulo_dos':titulo_dos, 'titulo_tres':titulo_tres, 'titulo_izq':titulo_izq, 'desc_uno':desc_uno, 'desc_dos':desc_dos, 'desc_tres':desc_tres, 'desc_izq':desc_izq, 'imagen_dos':imagen_dos, 'imagen_tres':imagen_tres})

	encabezado = Encabezado.objects.all()
	listaEn = []
	for a in encabezado:
		imagen_encabezado_acerca = str(a.imagen_encabezado_acerca)
		listaEn.append({'imagen_encabezado_acerca':imagen_encabezado_acerca})

	ctx = {'about':listaAb, 'encabezado':listaEn}
	return render(request, 'about.html', ctx)

def contact(request):
	contact = Contactenos.objects.all()
	ctx = {'contact':contact}
	return render(request, 'contact.html', ctx)

def history(request):
	history = About.objects.all()
	listaHis =[]
 	for a in history:
		imagen_historia= str(a.imagen_historia)
		imagen_encabezado_historia= str(a.imagen_encabezado_historia)
		descripcion_historia = a.descripcion_historia
		imagen_firma = str (a.imagen_firma)
		listaHis.append({'imagen_historia':imagen_historia, 'imagen_firma':imagen_firma, 'descripcion_historia':descripcion_historia, 'imagen_encabezado_historia':imagen_encabezado_historia})

	encabezado = Encabezado.objects.all()
	listaEn = []
	for a in encabezado:
		imagen_encabezado_acerca = str(a.imagen_encabezado_acerca)
		listaEn.append({'imagen_encabezado_acerca':imagen_encabezado_acerca})
	ctx = {'history':listaHis,'encabezado':listaEn}
	return render(request, 'history.html', ctx)

def news(request):
	news1 = Noticia.objects.all().count()
	print news1
	news = Noticia.objects.all()

	listaNot =[]
 	for a in news:
		id_noticia= a.id_noticia
		imagen = str(a.imagen)
		titulo = a.titulo
		fecha = a.fecha
		contenido = a.contenido
		descripcion = a.descripcion
		palabras_clave = a.palabras_clave
		listaNot.append({ 'id_noticia':id_noticia,'imagen':imagen, 'titulo':titulo, 'fecha':fecha, 'contenido':contenido, 'descripcion':descripcion, 'palabras_clave':palabras_clave})

	encabezado = Encabezado.objects.all()
	listaEn = []
	for y in encabezado:
		imagen_encabezado_noticia = str(y.imagen_encabezado_noticia)
		listaEn.append({'imagen_encabezado_noticia':imagen_encabezado_noticia})

	ctx = {'news':listaNot, 'encabezado':listaEn, 'news1':news1}
	return render(request, 'news-list.html', ctx)

def json_noticias(request,inf,sup):
	listaNot =[]

	news = Noticia.objects.filter(id_noticia__range=(inf, sup))
	for a in news:
		id_noticia= a.id_noticia
		imagen = str(a.imagen)
		titulo = a.titulo
		fecha = str(a.fecha)
		contenido = a.contenido
		descripcion = a.descripcion
		listaNot.append({ 'id_noticia':id_noticia,'imagen':imagen, 'titulo':titulo, 'fecha':fecha, 'contenido':contenido, 'descripcion':descripcion})

	return HttpResponse(json.dumps(listaNot), content_type='application/json')


def single_news(request, pk):
	print str(pk)
	single_news = Noticia.objects.filter(id_noticia=pk)
	listaNotS =[]
 	for a in single_news:
 		id_noticia= a.id_noticia
		imagen = str(a.imagen)
		titulo = a.titulo
		fecha = a.fecha
		contenido = a.contenido
		descripcion = str(a.descripcion)
		palabras_clave = a.palabras_clave
		listaNotS.append({ 'id_noticia':id_noticia,'imagen':imagen, 'titulo':titulo, 'fecha':fecha, 'contenido':contenido, 'descripcion':descripcion, 'palabras_clave':palabras_clave})

	encabezado = Encabezado.objects.all()
	listaEn = []
	for a in encabezado:
		imagen_encabezado_noticia = str(a.imagen_encabezado_noticia)
		listaEn.append({ 'imagen_encabezado_noticia':imagen_encabezado_noticia})
	ctx = {'single_news':listaNotS, 'encabezado':listaEn}
	return render(request, 'single-news.html', ctx)

def list_team(request,id_equipo):
	print(id_equipo)
	lista_per=[]
	lista_team=[]
	list_team= EquipoMiembro.objects.filter(id_equipo=id_equipo)
	for a in list_team:
		identificado=a.id_equipo
		imagen=str(a.imagen)
		nombre = a.nombre
		facebok = a.facebok
		twitter = a.twitter
		especialidad = a.especialidad
		titulo_descripcion =a.titulo_descripcion
		descripcion = a.descripcion
		lista_per.append({'identificador':identificado,'imagen':imagen,'nombre':nombre,'facebok':facebok, 'twitter':twitter, 'especialidad':especialidad, 'descripcion':descripcion, 'titulo_descripcion':titulo_descripcion})

	team=EquipoMiembro.objects.all()
	for a in team:
		identificado=a.id_equipo
		imagen = str(a.imagen)
		nombre = a.nombre
		facebok = a.facebok
		twitter = a.twitter
		especialidad = a.especialidad
		titulo_descripcion =a.titulo_descripcion
		descripcion = a.descripcion
		lista_team.append({'identificador':identificado,'imagen':imagen, 'nombre':nombre, 'especialidad':especialidad,})

	ctx={'lista_per':lista_per,'lista_team':lista_team}

	return render(request, 'staff-single.html', ctx)

def staff_single(request):
	# print str(pk)
	staff_single = EquipoMiembro.objects.filter(id_equipo=1)
	lista =[]
 	for a in staff_single:
 		id_equipo = a.id_equipo
		imagen = str(a.imagen)
		nombre = a.nombre
		facebok = a.facebok
		twitter = a.twitter
		especialidad = a.especialidad
		titulo_descripcion =a.titulo_descripcion
		descripcion = a.descripcion
		lista.append({'id_equipo':id_equipo, 'nombre':nombre, 'imagen':imagen, 'facebok':facebok, 'twitter':twitter, 'especialidad':especialidad, 'descripcion':descripcion, 'titulo_descripcion':titulo_descripcion})
	# print(lista)
	# encabezado = Encabezado.objects.all()
	# listaEn = []
	# for a in encabezado:
	# 	imagen_encabezado_acerca = str(a.imagen_encabezado_acerca)[7:]
	# 	listaEn.append({'imagen_encabezado_acerca':imagen_encabezado_acerca})

	team = EquipoMiembro.objects.all()
	listaEq =[]
 	for a in team:
 		id_equipo = a.id_equipo
		imagen = str(a.imagen)
		nombre = a.nombre
		facebok = a.facebok
		twitter = a.twitter
		especialidad = a.especialidad
		titulo_descripcion =a.titulo_descripcion
		descripcion = a.descripcion
		listaEq.append({'id_equipo':id_equipo,'imagen':imagen, 'nombre':nombre, 'especialidad':especialidad, 'facebok':facebok, 'twitter':twitter})

	encabezado = Encabezado.objects.all()
	listaEn = []
	for a in encabezado:
		imagen_encabezado_acerca = str(a.imagen_encabezado_acerca)
		listaEn.append({ 'imagen_encabezado_acerca':imagen_encabezado_acerca})



	ctx = {'staff_single':lista,'team':listaEq, 'encabezado':listaEn}
	return render(request, 'staff-single.html', ctx)

def team(request):
	team = EquipoMiembro.objects.all()
	listaTeam =[]
 	for a in team:
		imagen = str(a.imagen)
		print imagen
		nombre = a.nombre
		facebok = a.facebok
		twitter = a.twitter
		especialidad = a.especialidad
		titulo_descripcion =a.titulo_descripcion
		descripcion = a.descripcion
		listaTeam.append({'nombre':nombre, 'imagen':imagen, 'facebok':facebok, 'twitter':twitter, 'especialidad':especialidad, 'descripcion':descripcion, 'titulo_descripcion':titulo_descripcion})

	encabezado = Encabezado.objects.all()
	listaEn = []
	for a in encabezado:
		imagen_encabezado_acerca = str(a.imagen_encabezado_acerca)
		listaEn.append({'imagen_encabezado_acerca':imagen_encabezado_acerca})

	ctx = {'team':listaTeam,'encabezado':listaEn}
	return render(request, 'team.html', ctx)


def services(request):
	services = Area.objects.all()
	listaServ =[]
 	for a in services:
 		imagen_id= a.pk
		imagen_uno = str(a.imagen_uno)
		imagen_dos = str(a.imagen_dos)
		imagen_tres = str(a.imagen_tres)
		nombre_area = a.nombre_area
		descripcion = a.descripcion
		print(imagen_id)
		listaServ.append({'imagen_id':imagen_id,'imagen_uno':imagen_uno, 'imagen_dos':imagen_dos, 'imagen_tres':imagen_tres, 'nombre_area':nombre_area, 'descripcion':descripcion})

	encabezado = Encabezado.objects.all()
	listaEn = []
	for a in encabezado:
		imagen_encabezado_area = str(a.imagen_encabezado_area)
		listaEn.append({'imagen_encabezado_area':imagen_encabezado_area})

	ctx = {'services':listaServ, 'encabezado':listaEn}
	return render(request, 'services.html', ctx)

def sendEmail(request):
    if request.method == 'POST' :
    	nombre = request.POST['nombre']
    	direccion = request.POST['direccion']
    	telefono = request.POST['telefono']
    	email = request.POST['email']
    	mensaje = request.POST['mensaje']
    	subject, from_email, to = 'La siguiente persona desea contactarse con ud.', 'mggroup@solnustec.com', 'miguelcapa20@gmail.com'
        text_content = 'Contactarse'
        html_content = '<p><strong>La siguiente persona desea contactarse con ud.:</strong><br> nombre: '+nombre+'<br> direccion:'+direccion+'<br> telefono:'+telefono+' <br> email: '+email+' <br> mensaje: '+mensaje

        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

	contact = Contactenos.objects.all()
	ctx = {'contact':contact}
    return render (request, 'contact.html', ctx)

def sendEmailA(request):
    if request.method == 'POST' :
    	nombre = request.POST['nombre']
    	telefono = request.POST['telefono']
    	email = request.POST['email']
    	tipo = request.POST['tipo']
    	subject, from_email, to = 'La siguiente persona desea contactarse con ud.', 'mggroup@solnustec.com', 'miguelcapa20@gmail.com'
        text_content = 'Contactarse'
        html_content = '<p><strong>La siguiente persona desea contactarse con ud.:</strong><br> nombre: '+nombre+'<br> tipo:'+tipo+'<br> telefono:'+telefono+' <br> email: '+email

        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

	contact = Contactenos.objects.all()
	# area = Area.objects.all()
	# parallax = Parallax.objects.all()
	# seccion = SeccionUno.objects.all()
	# parallaxdos = ParallaxDos.objects.all()
	# equipo = EquipoMiembro.objects.all()
	# header = HeaderImagen.objects.all()
	
	# ctx = {'header':header,'area':area, 'parallax':parallax, 'parallaxdos':parallaxdos, 'equipo':equipo, 'seccion':seccion}
	ctx = {'contact':contact}
    return render (request, 'contact.html', ctx)